

import serial
import socket
import re
from threading import Thread
from SocketServer import ThreadingMixIn
from BaseHTTPServer import HTTPServer, BaseHTTPRequestHandler
from ctypes import * 

class Handler(BaseHTTPRequestHandler):

	def do_GET(self):		
		self.send_response(200)
		self.send_header("Content-type","text/html")
		self.send_header("Access-Control-Allow-Origin","*")
		self.end_headers()
		weight = readweight()
		self.wfile.write(weight)

	def log_message(self, format, *args):		
		print "LOG: " + self.address_string()	

class ThreadingHTTPServer(ThreadingMixIn,HTTPServer):
	pass

def server_on(port):
	ip = socket.gethostbyname(socket.gethostname())
	server = ThreadingHTTPServer((ip,port),Handler)
	server.serve_forever()	

def readweight():
	try:
		port = serial.Serial('COM1',2400)
		port.timeout = 8
		port.bytesize = serial.EIGHTBITS
		port.parity = serial.PARITY_NONE
		port.stopbits = serial.STOPBITS_ONE

		weight = port.read(8)

		pattern = re.compile('\d+')
		m  = pattern.search(weight)

		weight =  m.group()
		port.close()
	except serial.SerialException:
		return 0
	except AttributeError:
		return 0		

	return weight

def readweight2():	
	##Carrega DLL da Balanca
	balance = WinDLL("BalancaLider.dll");
	b = balance.CapturarPeso(c_int(1),c_int(2600))
	print b

def main():
	print "Executando servidor web."
	Thread(target=server_on,args=[1111]).start()
	server_on(80)	
	readweight2()

if __name__ == '__main__':
	#print readweight()
	main()